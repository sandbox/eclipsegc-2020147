<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function rocketbooster_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks().
 */
function rocketbooster_install_tasks() {
  $task = array();
  $task['rocketbooster_custom_settings'] = array(
    'display_name' => st('Rocketship'),
    'display' => TRUE,
    'type' => 'form',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
  );
  return $task;
}

/**
 * A configuration form for rocketship variables.
 */
function rocketbooster_custom_settings($form, $form_state) {
  $form['rocketship_master_tag'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Master Tag'),
    '#description' => t('The drupal.org tag to use as a master tag for all issue tracking.'),
  );
  $form['rocketship_project'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Drupal.org Project'),
    '#description' => t('The drupal.org project for the issues.'),
    '#default_value' => 'drupal',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Finish'),
  );
  return $form;
}

function rocketbooster_custom_settings_submit(&$form, &$form_state) {
  variable_set('rocketship_master_tag', $form_state['values']['rocketship_master_tag']);
  variable_set('rocketship_project', $form_state['values']['rocketship_project']);
}

/**
 * Implements hook_ctools_plugin_api().
 */
function rocketbooster_ctools_plugin_api($module, $api) {
  if ($module == 'page_manager' && $api == 'pages_default') {
    return array('version' => 1);
  }
}

/**
 * Implements hook_views_api().
 */
function rocketbooster_views_api($module, $api) {
  if ($module == 'views' && $api == 'views_default') {
    return array('version' => 2);
  }
}

