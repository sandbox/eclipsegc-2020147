; Drupal.org release file.
core = 7.x
api = 2

projects[rocketship][subdir] = contrib
projects[rocketship][type] = module
projects[rocketship][download][type] = git
projects[rocketship][download][url] = http://git.drupal.org/sandbox/goba/1470484.git
projects[rocketship][download][branch] = master
projects[rocketship][patch][] = "https://drupal.org/files/1986868-5.patch"

projects[ctools][subdir] = contrib
projects[ctools][type] = module
projects[ctools][download][type] = git
projects[ctools][download][url] = http://git.drupal.org/project/ctools.git
projects[ctools][download][branch] = 7.x-1.x
projects[ctools][patch][] = "http://drupal.org/files/page-manager-admin-paths_1.patch"

projects[context_admin][subdir] = contrib
projects[context_admin][type] = module
projects[context_admin][version] = 1.2

projects[panels][subdir] = contrib
projects[panels][type] = module
projects[panels][version] = 3.3

projects[views][subdir] = contrib
projects[views][type] = module
projects[views][version] = 3.7

