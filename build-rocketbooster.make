api = 2
core = 7.x
projects[drupal][version] = 7.22
; Download the install profile and recursively build all its dependencies:
projects[rocketbooster][type] = profile
projects[rocketbooster][download][type] = git
projects[rocketbooster][download][url] = http://git.drupal.org/sandbox/eclipsegc/2020147.git
projects[rocketbooster][download][branch] = 7.x-1.x
